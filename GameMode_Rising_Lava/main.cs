function RL_Reload()
{
	exec("Add-Ons/GameMode_Rising_Lava/main.cs");
	messageAll('', "\c6The Rising Lava code has been reloaded.");
}

//		INFORMATION CLOCK ROTATION
$RL_InfoClockMinutes = 5;

function RL_StartInfoClock()
{
	echo("Started the Rising Lava info clock.");
	schedule(500, 0, RL_InfoClockLoop);
}

function RL_DoBrickTags(%name)
{
	%client = findClientByName(%name);
	%client.clanPrefix = "<bitmap:base/data/shapes/brickTOP>";
	%client.clanSuffix = "<bitmap:base/data/shapes/brickTOP>";
}

function RL_InfoClockLoop()
{
	%MessageCount = 0;
	%Message[%MessageCount++] = "Report all hackers on the <a:zapkraft.netne.net/forum>forums</a>!";
	%Message[%MessageCount++] = "Want to apply for admin or make a suggestion? Register for our <a:zapkraft.netne.net/forum>forums</a>!";
	%Message[%MessageCount++] = "We're open to user-made maps! They just need to have default bricks and play/look well!";
	%Message[%MessageCount++] = "Did you just die? No need to be upset, it's merely a game!";
	%Message[%MessageCount++] = "Don't block the stairs every round, sacrificing everyone is not cool.";
	echo("Rising Lava: Pushed info message.");
	announce("\c4Server Info\c6:" SPC %Message[getRandom(1, %MessageCount)]);
	$RL_InfoClockLoop = schedule($RL_InfoClockMinutes * 60000, 0, RL_InfoClockLoop);
}

function RL_StopInfoClock()
{
	echo("Stopped the Rising Lava info clock.");
	cancel($RL_InfoClockLoop);
}

//		MAIN GAME MODE CODE
function RL_BuildLevelList()
{
	//
	%pattern = "Add-Ons/RisingLava_*/save.bls";
	
	$RL::numLevels = 0;
	
	%file = findFirstFile(%pattern);
	while(%file !$= "")
	{
		$RL::Level[$RL::numLevels] = %file;
		$RL::numLevels++;

		%file = findNextFile(%pattern);
	}
}

function RL_DumpLevelList()
{
	echo("");
	if($RL::numLevels == 1)
		echo("1 level");
	else
		echo($RL::numLevels @ " levels");
	for(%i = 0; %i < $RL::numLevels; %i++)
	{
		%displayName = $RL::Level[%i];
		%displayName = strReplace(%displayName, "Add-Ons/RisingLava_", "");
		%displayName = strReplace(%displayName, "/save.bls", "");
		%displayName = strReplace(%displayName, "_", " ");

		if(%i == $RL::CurrentLevel)
			echo(" >" @ %displayName);
		else
			echo("  " @ %displayName);
	}
	echo("");
}

function RL_NextLevel()
{
	$RL::CurrentLevel = mFloor($RL::CurrentLevel);
	$RL::CurrentLevel++;
	$RL::CurrentLevel = $RL::CurrentLevel % $RL::numLevels;

	$RL::ResetCount = 0;

	RL_LoadLevel_Phase1($RL::Level[$RL::CurrentLevel]);
}

function RL_LoadLevel_Phase1(%filename)
{
	//suspend minigame resets
	$RL::MapChange = 1;

	//put everyone in observer mode
	%mg = $DefaultMiniGame;
	if(!isObject(%mg))
	{
		error("ERROR: RL_LoadLevel( " @ %filename  @ " ) - default minigame does not exist");
		return;
	}
	for(%i = 0; %i < %mg.numMembers; %i++)
	{
		%client = %mg.member[%i];
		%player = %client.player;
		if(isObject(%player))
			%player.delete();

		%camera = %client.camera;
		%camera.setFlyMode();
		%camera.mode = "Observer";
		%client.setControlObject(%camera);
	}
	
	//clear all bricks 
	// note: this function is deferred, so we'll have to set a callback to be triggered when it's done
	BrickGroup_888888.chaindeletecallback = "RL_LoadLevel_Phase2(\"" @ %filename @ "\");";
	BrickGroup_888888.chaindeleteall();
}

function RL_LoadLevel_Phase2(%filename)
{
	echo("Loading rising lava save " @ %filename);

	%displayName = %filename;
	%displayName = strReplace(%displayName, "Add-Ons/RisingLava_", "");
	%displayName = strReplace(%displayName, "/save.bls", "");
	%displayName = strReplace(%displayName, "_", " ");
	
	%loadMsg = "\c6Now loading \c4" @ %displayName;

	//read and display credits file, if it exists
	// limited to one line
	%creditsFilename = filePath(%fileName) @ "/credits.txt";
	if(isFile(%creditsFilename))
	{
		%file = new FileObject();
		%file.openforRead(%creditsFilename);

		%line = %file.readLine();
		%line = stripMLControlChars(%line);
		%loadMsg = %loadMsg @ "\c6, created by \c4" @ %line;

		%file.close();
		%file.delete();
	}

	messageAll('', %loadMsg);
	
	RL_PlayersCanVote(true);

	//load environment if it exists
	%envFile = filePath(%fileName) @ "/environment.txt"; 
	if(isFile(%envFile))
	{  
		//echo("parsing env file " @ %envFile);
		//usage: GameModeGuiServer::ParseGameModeFile(%filename, %append);
		//if %append == 0, all minigame variables will be cleared 
		%res = GameModeGuiServer::ParseGameModeFile(%envFile, 1);

		EnvGuiServer::getIdxFromFilenames();
		EnvGuiServer::SetSimpleMode();

		if(!$EnvGuiServer::SimpleMode)	  
		{
			EnvGuiServer::fillAdvancedVarsFromSimple();
			EnvGuiServer::SetAdvancedMode();
		}
	}
	
	%cfgFile = filePath(%fileName) @ "/config.txt"; 
	if(isFile(%cfgFile))
	{  
	echo("Loading custom Rising Lava config file.");
	%file = new fileObject();
	
	%file.openForRead(filePath(%fileName) @ "/config.txt");
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		if(firstWord(%line) $= "MAX_LAVA_HEIGHT")
		{
			$RL_MaxLavaHeight = restWords(%line);
		}
	}
	%file.close();
	%file.delete();
	}
	else
	{
	 $RL_MaxLavaHeight = 20;
	}
	
	//load save file
	schedule(10, 0, serverDirectSaveFileLoad, %fileName, 3, "", 2, 1);
	
	RL_VS_CancelVote();
}


//		COMMANDS FOR THE MANS
function serverCmdLevelList(%client)
{
	for(%i = 0; %i < $RL::numLevels; %i++)
	{
		%displayName = $RL::Level[%i];
		%displayName = strReplace(%displayName, "Add-Ons/RisingLava_", "");
		%displayName = strReplace(%displayName, "/save.bls", "");
		%displayName = strReplace(%displayName, "_", " ");

		if(%i == $RL::CurrentLevel)
			messageClient(%client, '', "\c4" @ %i @ "\c6. " @ %displayName SPC "(\c4Selected\c6)");
		else
			messageClient(%client, '', "\c4" @ %i @ "\c6. " @ %displayName);
	}
}

function servercmdReloadLevels(%client)
{
	if(!%client.isSuperAdmin)
	{
		messageClient(%client, '', "\c6You are not super admin!");
		return;
	}
	messageAll('', "\c4" @ %client.name SPC "\c6reloaded the Rising Lava level cache.");
	setModPaths(getModPaths());
	RL_BuildLevelList();
	%levelDir = $RL::Level[$RL::CurrentLevel];
	%cfgFile = filePath(%levelDir) @ "/config.txt"; 
	if(isFile(%cfgFile))
	{  
	echo("Loading custom Rising Lava config file.");
	messageAll('', "\c4" @ %client.name SPC "\c6reloaded the current level's config.");
	%file = new fileObject();
	
	%file.openForRead(filePath(%cfgFile) @ "/config.txt");
	while(!%file.isEOF())
	{
		%line = %file.readLine();
		if(firstWord(%line) $= "MAX_LAVA_HEIGHT")
		{
			$RL_MaxLavaHeight = restWords(%line);
		}
	}
	%file.close();
	%file.delete();
	}
}
function serverCmdSetLevel(%client, %i)
{
	if(!%client.isAdmin)
		return;

	if(mFloor(%i) !$= %i)
	{
		messageClient(%client, '', "Usage: /setLevel <number>");
		return;
	}

	if(%i < 0 || %i > $RL::numLevels)
	{
		messageClient(%client, '', "serverCmdSetLevel() - out of range index");
		return;
	}

	messageAll( 'MsgAdminForce', '\c4%1\c6 changed the level', %client.getPlayerName());
	
	$RL::CurrentLevel = %i - 1;
	RL_NextLevel();
}

function serverCmdNextLevel(%client, %i)
{
	if(!%client.isAdmin)
		return;

	messageAll( 'MsgAdminForce', '\c4%1\c6 changed the level', %client.getPlayerName());
	
	RL_NextLevel();
}

function serverCmdRules(%client)
{
	%title = "Rising Lava Rules";
	%rules = "<just:left><font:arial:15>1. Do not provoke or bully other users.";
	%rules = %rules NL "2. Do not block other users before the round has begun.";
	%rules = %rules NL "3. Do not post pornographic links.";
	%rules = %rules NL "4. Do not spam the chat.";
	%rules = %rules NL "Failure to follow these rules will be subject to an admin's judgement.";
	
	commandToClient(%client, 'MessageBoxOK', %title, %rules);
}

//		PACKAGE
deactivatepackage(GameModeRisingLavaPackage);
package GameModeRisingLavaPackage
{
	function gameConnection::onClientEnterGame(%client)
	{
		parent::onClientEnterGame(%client);
		schedule(1000, 0, serverCmdRules, %client);
	}
	//		GREENLIGHT STUFF
	function gameConnection::spawnPlayer(%client)
	{
		parent::spawnPlayer(%client);
	}
	function serverCmdLight(%client)
	{
		parent::serverCmdLight(%client);
	}
	function serverCmdMessageSent(%client,%msg)
	{
		if(%client.isAdmin)
		{
			%client.clanSuffix = "";
			%client.clanPrefix = "\c6[\c4A\c6] ";
			if(%client.isSuperAdmin)
			{
				%client.clanPrefix = "\c6[\c4SA\c6] ";
				if(%client.bl_id == getNumKeyID())
				{
					%client.clanPrefix = "\c6[\c4Host\c6] ";
				}
			}
		}
		Parent::serverCmdMessageSent(%client,%msg);
	}
	function Player::activateStuff(%player)
	{
		Parent::activateStuff(%player);
	}
	function GameConnection::OnDeath(%client, %killerPlayer, %killer, %damageType, %damageLoc)
	{
		//		GREENLIGHT STUFF
		if(isObject(%client.minigame))
		{
			%peopleAlive = -1;
			$RL_PeopleAllive = -1;
			for(%a = 0; %a < %client.minigame.numMembers; %a++)
			{
				%miniPlayer = %client.minigame.member[%a];
				if(isObject(%miniPlayer.player))
				{
					%peopleAlive++;
					$RL_PeopleAllive++;
					%lastDetectedPlayer = %miniPlayer;
				}
			}
			echo("Rising Lava: " @ %peopleAlive @ " players left.");
			if(%peopleAlive == 1)
			{
				RL_CancelLoops();
				serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 1);
				schedule(100, 0, serverCmdEnvGui_SetVar, RL_Env, "WaterHeight", 0);
				echo("Rising Lava: We have a winner!");
				bottomPrintAll("<font:impact:25>\c4" @ %lastDetectedPlayer.name SPC "\c6wins!", 3, 1);
				%lastDetectedPlayer.addScore(1);
				RL_PlayWinSound();
			}
		}
		%lastAttacker = %client.lastAttacker;
		if(%lastAttacker != 0)
		{
			messageClient(%client, '', "\c6You were killed by \c4" @ %lastAttacker.name @ "\c6!");
			%lastAttacker.addScore(1);
			messageClient(%lastAttacker, '', "\c6You have killed \c4" @ %client.name @ "\c6!");
			echo("Rising Lava:" SPC %lastAttacker.name SPC "killed" SPC %client.name);
		}
		parent::OnDeath(%client, %killerPlayer, %killer, %damageType, %damageLoc);
	}
	function fxDTSBrick::setVehicle(%obj, %data, %client)
	{
	  return;
	}
	function fxDTSBrick::setItem(%obj, %data, %client)
	{
	  return;
	}
	//this is called when save loading finishes
	function GameModeInitialResetCheck()
	{
		RL_CancelLoops();
		serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 1);
		serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 0);
		Parent::GameModeInitialCheck();

		//if there is no level list, attempt to create it
		if($RL::numLevels == 0)
			RL_BuildLevelList();
		
		//if levellist is still empty, there are no levels
		if($RL::numLevels == 0)
		{
			messageAll('', "\c6No RisingLava levels available!");
			return;
		}

		if($RL::Initialized)
			return;

		$RL::Initialized = true;
		$RL::CurrentLevel = -1;
				
		RL_NextLevel();
	}

	//when we're done loading a new level, reset the minigame
	function ServerLoadSaveFile_End()
	{
		Parent::ServerLoadSaveFile_End();

		//new level has loaded, reset minigame
		if($DefaultMiniGame.numMembers > 0) //don't bother if no one is here (this also prevents starting at round 2 on server creation)
			$DefaultMiniGame.scheduleReset(); //don't do it instantly, to give people a little bit of time to ghost
	}
	function MiniGameSO::Reset(%obj, %client)
	{
		RL_CancelLoops();
		serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 1);
		serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 0);
		//make sure this value is an number
		$Pref::Server::RisingLava::RoundLimit = mFloor($Pref::Server::RisingLava::RoundLimit);


		//count number of minigame resets, when we reach the limit, go to next level
		if(%obj.numMembers >= 0)
		{
			$RL::ResetCount++;
		}

		if($RL::ResetCount > $Pref::Server::RisingLava::RoundLimit)
		{
			$RL::ResetCount = 0;
			RL_NextLevel();
		}
		else
		{
			messageAll('', "\c6Beginning round \c4" @ $RL::ResetCount @ " \c6of \c4" @ $Pref::Server::RisingLava::RoundLimit);
		 
		 Parent::Reset(%obj, %client);
		}
	  //cancel($RL_MessageSchedule);
	  RL_StartLavaSequence();
	  RL_LoadPlayerScores();
	}
	function MiniGameSO::chatMessageAll(%obj, %client, %msg)
	{
		%msg = strReplace(%msg, "\c3", "\c4");
		%msg = strReplace(%msg, "\c5", "\c6");
		//		GREENLIGHT STUFF
		%msg = strReplace(%msg, "is the last man standing!", "has survived the Rising Lava!");
		Parent::chatMessageAll(%obj, %client, %msg);
	}
	function Quit()
	{
		RL_SavePlayerScores();
		Parent::Quit();
	}
	function pushBroomProjectile::onCollision(%this, %obj, %col, %vec, %vecLen, %that, %potatoe, %sexual, %farts)
	{
		parent::onCollision(%this, %obj, %col, %vec, %vecLen, %that, %potatoe, %sexual, %farts);
		%cl = %col.client;
		%source = %obj.sourceObject.client;
		%cl.lastAttacker = %source;
		cancel(%cl.lastAttackSched);
		if(isObject(%cl.player))
		{
			%cl.lastAttackSched = schedule(10000, 0, RL_resetLastAttacker, %cl);
		}
	}

};
activatePackage(GameModeRisingLavaPackage);

function RL_resetLastAttacker(%cl)
{
	cancel(%cl.lastAttackSched);
	%cl.lastAttacker = 0;
}

function RL_SavePlayerScores()
{
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		if(%client.score > 0)
		{
			$RL_PlayerScore_[%client.BL_ID] = %client.score;
		}
	}
	export("$RL_PlayerScore_*", "config/server/Rising Lava/score.cs");
}

function GameConnection::addScore(%this, %add)
{
	%this.score += %add;
	$RL_PlayerScore_[%this.BL_ID] = %this.score;
	export("$RL_PlayerScore_*", "config/server/Rising Lava/score.cs");
	
	if(%this.score / 50 == mFloor(%this.score / 50))
	{
		messageAll('', "\c4" @ %this.name SPC "\c6has reached level\c4" SPC %this.score @ "\c6!");
	}
}

function RL_PlayWinSound()
{
	serverPlay2D(Synth_09_Sound);
	schedule(500, 0, serverPlay2D, Synth_10_Sound);
	schedule(1000, 0, serverPlay2D, Synth_11_Sound);
}

function RL_LoadPlayerScores()
{
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		%client.score = $RL_PlayerScore_[%client.BL_ID];
	}	
}

function DumpPlayerScores()
{
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		echo(%client.score SPC "-" SPC %client.name);
	}
}

//		LAVA CODE
function RL_StartLavaSequence()
{
	if(!isObject(RL_Env))
	{
		new AIConnection(RL_Env)
		{
			isAdmin = 1;
			isSuperAdmin = 1;
			bl_id = 1337;
		};
	}
	serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 1);
	serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", 0);
	
	$Pref::Server::RisingLava::PreRound = mFloor($Pref::Server::RisingLava::PreRound); 
	
	%times = 1;
	%seconds = $Pref::Server::RisingLava::PreRound;
	while(%times <= $Pref::Server::RisingLava::PreRound + 1)
	{
		if(%times == $Pref::Server::RisingLava::PreRound + 1)
		{
			$RL_MessageSchedule[%times] = schedule(%times * 1000, 0, messageAll, 'MsgAdminForce', "\c6The lava has began to rise! Players have been given \c4Push Brooms\c6.");
			PushbroomItem.canDrop = false;
			$RL_StartRisingSequenceSchedule = schedule(%times * 1000, 0, RL_StartRisingSequnce);
		}
		$RL_RoundStartBPSchedule[%times] = schedule(%times * 1000, false, bottomPrintAll, "<font:impact:25>\c6Starting In:\c4" SPC %seconds, 2, 1);
		if(%seconds == $Pref::Server::RisingLava::PreRound || %seconds == 30 || %seconds == 20 || %seconds <= 10)
		{
			if(%seconds > 0)
			{
				$RL_MessageSchedule[%times] = schedule(%times * 1000, 0, messageAll, '', "\c6The lava will rise in \c4" @ %seconds @ "\c6 seconds.");
			}
		}
		%times++;
		%seconds--;
	}
}

function RL_StartRisingSequnce()
{
	if(!isObject(RL_Env))
	{
		new AIConnection(RL_Env)
		{
			isAdmin = 1;
			isSuperAdmin = 1;
			bl_id = 1337;
		};
	}
	RL_LavaContinue();
	RL_BeginSpeedTime();
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		%client.player.tool[0] = PushBroomItem.getID();
		messageClient(%client,'MsgItemPickup','',0,PushBroomItem.getID());
	}
	$RL_LavaSpeed = $RL::ResetCount;
	messageAll('', "\c6Lava Speed:\c4" SPC $RL_LavaSpeed);
	
	if($RL_HyperRoundCount == 50)
	{
		messageAll('', "\c4Hyper Round\c6! The lava this round is 10x the normal speed!");
		$RL_LavaSpeed = $RL_LavaSpeed * 10;
		$RL_HyperRoundCount = 0;
	}
	$RL_HyperRoundCount++;
}

function RL_BeginSpeedTime()
{
	cancel($RL_SpeedTime);
	$RL_SpeedTime = schedule(60000, 0, RL_SpeedTime);
}

function RL_SpeedTime()
{
	cancel($RL_SpeedTime);
	serverPlay2D(BrickPlantSound);
	$RL_LavaSpeed += 1;
	$RL_SpeedTime = schedule(30000, 0, RL_SpeedTime);
}

function RL_LavaContinue(%a)
{
	cancel($RL_LavaContinue);
	if(%a >= 999)
	{
		return;
	}
	
	serverCmdEnvGui_SetVar(RL_Env, "WaterHeight", %a);
	
	//		GREENLIGHT STUFF
	
	%font = "<font:impact:25>";
	%level = RL_GetLevelName($RL::CurrentLevel);
	%height = mFloatLength(%a, 2);
	
	%players = $RL_PeopleAllive;
	if(%players <= 1)
	{
		%players = "All";
	}
	
	RL_DoLavaMessage(%a);
	//bottomPrintAll(%font @ "<just:center>\c4Map:\c6" SPC %level SPC "\c4Round:\c6" SPC $RL::ResetCount @ "/" @ $Pref::Server::RisingLava::RoundLimit NL "\c4Speed:\c6" SPC $RL_LavaSpeed SPC "\c4Players:\c6" SPC %players SPC "\c4Level:\c6" SPC %height, 2, 1);
	//for(%a = 0; %a < ClientGroup.getCount(); %a++)
	//{
	//	%client = ClientGroup.getObject(%a);
	//	bottomPrint(%client, %font @ "<just:center>\c4Map:\c6" SPC %level SPC "\c4Round:\c6" SPC $RL::ResetCount @ "/" @ $Pref::Server::RisingLava::RoundLimit NL "\c4Speed:\c6" SPC $RL_LavaSpeed SPC "\c4Players:\c6" SPC %players SPC "\c4Level:\c6" SPC %height, 2, 1);
	//}
	
	%additive = $RL_LavaSpeed * 0.00332;
	
	$RL_LavaContinue = schedule(33, 0, RL_LavaContinue, %a + %additive);
}

function RL_DoLavaMessage(%a)
{
	%players = $RL_PeopleAllive;
	if(%players <= 1)
	{
		%players = "All";
	}
	
	%font = "<font:impact:25>";
	%level = RL_GetLevelName($RL::CurrentLevel);
	%height = mFloatLength(%a, 2);
	
	for(%a = 0; %a < ClientGroup.getCount(); %a++)
	{
		%client = ClientGroup.getObject(%a);
		
		if(!isObject(%client.player))
		{
			%position = "Heaven";
		}
		
		else
		{
			%position = getWord(%client.player.getPosition(), 2);
			%position = mFloor(%position);
			if(%position == 0)
			{
				%position = "Ground";
			}
		}
		
		//		GREENLIGHT STUFF
		bottomPrint(%client, %font @ "<just:center>\c4Map:\c6" SPC %level SPC "\c4Round:\c6" SPC $RL::ResetCount @ "/" @ $Pref::Server::RisingLava::RoundLimit NL "\c4Speed:\c6" SPC $RL_LavaSpeed SPC "\c4Players:\c6" SPC %players SPC "\c4Lava:\c6" SPC %height NL "\c4Name:\c6" SPC %client.name SPC "\c4Level:\c6" SPC %client.score SPC "\c4Height:\c6" SPC %position, 2, 1);
	}
}

function RL_GetLevelName(%num)
{
	%displayName = $RL::Level[%num];
	%displayName = strReplace(%displayName, "Add-Ons/RisingLava_", "");
	%displayName = strReplace(%displayName, "/save.bls", "");
	%displayName = strReplace(%displayName, "_", " ");
	return %displayName;
}

function RL_CancelLoops()
{
	for(%a = 1; %a <= $Pref::Server::RisingLava::PreRound + 1; %a++)
	{
		cancel($RL_MessageSchedule[%a]);
		cancel($RL_RoundStartBPSchedule[%a]);
	}
	cancel($RL_StartRisingSequenceSchedule);
	cancel($RL_LavaContinue);
	cancel($RL_SpeedTime);
}

function serverCmdCheck(%client, %target)
{
	%target = findClientByname(%target);
	if(!isObject(%target.player))
	{
		messageClient(%client, '', "\c6The player must be on the server and not dead!");
		return;
	}
	%target.savePing(1);
	%target.schedule(100, savePing, 2);
	%target.schedule(200, savePing, 3);
	schedule(300, 0, RL_CheckPhaseTwo, %client, %target);
}

function serverCmdWho(%client)
{
	messageClient(%client, '', "\c4Players Alive\c6:");
	for(%a = 0; %a < %client.minigame.numMembers; %a++)
	{
		%miniPlayer = %client.minigame.member[%a];
		if(isObject(%miniPlayer.player))
		{
			%num++;
			messageClient(%client, '', "\c4" @ %num @ "\c6. " @ %miniPlayer.name);
		}
	}
}

function RL_CheckPhaseTwo(%client, %target)
{
	for(%a=1;%a<=3;%a++)
	{
		%ping[%a] = %target.TempPing[%a];
	}
	messageClient(%client, '', "\c6Name: \c4" @ %target.name);
	messageClient(%client, '', "\c6Ping Checks: \c4" @ %ping1 @ "ms\c6, \c4" @ %ping2 @ "ms\c6, \c4" @ %ping3 @ "ms\c6.");
	if(%ping1 == %ping2 && %ping1 == %ping3)
	{
		messageClient(%client, '', "\c6They are just lagging, no hacks.");
	}
	else
	{
		messageClient(%client, '', "\c6They are not lagging. If they are floating it is most likely hacking!");
	}
}

function GameConnection::savePing(%this, %num)
{
	%this.TempPing[%num] = %this.getPing();
}

function serverCmdHelp(%client)
{
	messageClient(%client, '', "\c4Rising Lava \c6by \c4Zapk");
	messageClient(%client, '', "\c6/\c4help\c6 - Display a list of commands.");
	messageClient(%client, '', "\c6/\c4rules\c6 - See the server rules.");
	messageClient(%client, '', "\c6/\c4who\c6 - Prints a list of all living players.");
	messageClient(%client, '', "\c6/\c4setLevel [number]\c6 - Sets the level to the number given.");
	messageClient(%client, '', "\c6/\c4nextLevel\c6 - Skips the current level.");
	messageClient(%client, '', "\c6/\c4check [name]\c6 - Checks if a player is lagging.");
}

function Player::setLight(%obj, %light)
{
	if(isObject(%light))
	{
		if(isObject(%obj.light))
		{
			%obj.light.delete();
		}
	
		%obj.light = new FxLight()
		{
			dataBlock = %light;
			enable = true;
			iconsize = 1;
			player = %obj;
		};
	
		if(isObject(%obj.light))
		{
			%obj.light.attachToObject(%obj);
		}
	}
}
